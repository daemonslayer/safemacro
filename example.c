#include <stdio.h>

#include "safemacro.h"

/*
 * Suppose we have the following unsafe macros:
 */
#define MAX_UNSAFE(A, B) A >= B ? A : B
#define SUM_UNSAFE(A, B, C, D, E, F) A + B + C + D + E + F

/*
 * For macros with zero to six arguments, use SAFEMACRO like this (see the long comment in safemacro.h for more
 * information):
 */
#define MAX(A, B) SAFEMACRO(MAX_UNSAFE, A, B)
#define SUM(A, B, C, D, E, F) SAFEMACRO(SUM_UNSAFE, A, B, C, D, E, F)

/*
 * Support for macros with more than six arguments is left as an exercise for the reader! However, you may want to have
 * a look at _SAFEMACRO_EXPAND in safemacro.h.
 */

int main(void)
{
	printf("MAX(-1, 1) = %i\n", MAX(-1, 1));
	printf("SUM(0, 1, 2, 3, 4, 5) = %i\n", SUM(0, 1, 2, 3, 4, 5));
	return 0;
}
