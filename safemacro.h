/*
 * Copyright 2023 "daemonslayer" <daemonslayer@riseup.net>
 *
 * Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby
 * granted, provided that the above copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN
 * AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */

/*
 * safemacro.h - magically avoid double evaluation in function-like expression macros
 *
 * safemacro grew out of yak shaving expedition that started from a simple goal: write a simple MAX macro that compares
 * two numbers "correctly." As it turns out, macros in C have several properties that make this difficult. Let's start
 * with a naive attempt:
 *
 * #define MAX_UNSAFE(A, B) A >= B ? A : B
 *
 * Most C programmers know that macros defined this way should always wrap their arguments in parentheses, and also
 * parenthesize the entire expression, in order to avoid problems with operator precedence. (See [1] if you are
 * unfamiliar with this problem.) So, a better attempt might be:
 *
 * #define MAX_SAFER(A, B) ((A) >= (B) ? (A) : (B))
 *
 * and indeed, this approach is common, and good enough for most codebases. However, there is still a subtle issue:
 * MAX_SAFER's arguments are evaluated twice. (Actually, because the ternary operator short-circuits, only one of the
 * arguments is evaluated twice, but I digress...) If MAX_SAFER is used on an expression with side effects, like ++i or
 * do_something(j), this could lead to bugs.
 *
 * Luckily, safemacro.h exports a single macro, called SAFEMACRO, that takes any simple function-like macro that expands
 * to an expression (like MAX_UNSAFE, above), and makes it "safe". For example:
 * 
 * #define MAX(A, B) SAFEMACRO(MAX_UNSAFE, A, B)
 *
 * Now,
 *
 * MAX(++i, do_something(j))
 *
 * expands to
 *
 * ({ __auto_type _safemacro_var0 = (++i);
 *    __auto_type _safemacro_var1 = (do_something(j));
 *    _safemacro_var0 >= _safemacro_var1 ? _safemacro_var0 : _safemacro_var1; })
 *
 * MAX's arguments are evaluated exactly once, behaving just like an inline function would - except that it's a macro,
 * and it's "generic" in the sense that can be used on any type that has a >= operator; we don't need maxi, maxu, maxf,
 * etc.
 *
 * This works by combining ideas from several sources. [2] from the GCC documentation describes the problem and some
 * first-order solutions; [3] by Paul Fultz II gives us a method for doing recursion in the C preprocessor; [4] by David
 * Mazières uses __VA_OPT__ to make writing recursive macros sane; and [5] by Likai Liu shows us how to write hygienic
 * macros in C. safemacro utilizes many concepts that will be familiar to programmers from functional languages, like
 * apply, let, zip and unzip, and even alpha substitution.
 *
 * A note on portability: safemacro makes use of several features that are only available in GNU C or C2x (soon to be
 * known as C23): statement exprs, __auto_type, __VA_OPT__, and __COUNTER__. Statement expressions (the "({ a; b; })"
 * syntax) are essential and probably cannot be replaced. __auto_type could be replaced with typeof, but this would
 * still lead to double evaluation when an instance of a variably-modified type (e.g, a struct with a flexible array
 * member) is passed in.  __VA_OPT__ could be removed by utilizing ideas described in [3]. I do not think __COUNTER__
 * can be replaced without horrible hacks.
 * 
 * Finally, if you need to define safe macros with more than six arguments, _SAFEMACRO_EXPAND must be modified. You can
 * add as many invocations of _SAFEMACRO_EXPAND_ONCE as you need.
 *
 * [1]: https://stackoverflow.com/questions/7186504/c-macros-and-use-of-arguments-in-parentheses
 * [2]: https://gcc.gnu.org/onlinedocs/gcc/Statement-Exprs.html
 * [3]: https://github.com/pfultz2/Cloak/wiki/C-Preprocessor-tricks,-tips,-and-idioms
 * [4]: https://www.scs.stanford.edu/~dm/blog/va-opt.html
 * [5]: https://lifecs.likai.org/2016/07/c-preprocessor-hygienic-macros.html
 */

#ifndef _SAFEMACRO_H
#define _SAFEMACRO_H

#define _SAFEMACRO_EMPTY()
#define _SAFEMACRO_DEFER(...) __VA_ARGS__ _SAFEMACRO_EMPTY() ()

#define _SAFEMACRO_EXPAND_ONCE(...) __VA_ARGS__
#define _SAFEMACRO_EXPAND(...) \
	_SAFEMACRO_EXPAND_ONCE(_SAFEMACRO_EXPAND_ONCE(_SAFEMACRO_EXPAND_ONCE(_SAFEMACRO_EXPAND_ONCE(__VA_ARGS__))))

/*
 * _SAFEMACRO_ZIP_FRESH_NAMES(arg0, ...)
 * =>  _safemacro_var_0, arg0, ...
 */
#define _SAFEMACRO_ZIP_FRESH_NAMES_RECUR() _SAFEMACRO_ZIP_FRESH_NAMES_AUX
#define _SAFEMACRO_ZIP_FRESH_NAMES_AUX(ARG, ...) \
	_SAFEMACRO_FRESH_NAME() , ARG __VA_OPT__(, _SAFEMACRO_DEFER(_SAFEMACRO_ZIP_FRESH_NAMES_RECUR)(__VA_ARGS__))
#define _SAFEMACRO_ZIP_FRESH_NAMES(...) \
	_SAFEMACRO_EXPAND(_SAFEMACRO_ZIP_FRESH_NAMES_AUX(__VA_ARGS__))

/*
 * _SAFEMACRO_UNZIP_NAMES(name0, arg0, ...)
 * => name0, ...
 */
#define _SAFEMACRO_UNZIP_NAMES_RECUR() _SAFEMACRO_UNZIP_NAMES_AUX
#define _SAFEMACRO_UNZIP_NAMES_AUX(NAME, _, ...) \
	NAME __VA_OPT__(, _SAFEMACRO_DEFER(_SAFEMACRO_UNZIP_NAMES_RECUR)(__VA_ARGS__))
#define _SAFEMACRO_UNZIP_NAMES(...) \
	_SAFEMACRO_EXPAND(_SAFEMACRO_UNZIP_NAMES_AUX(__VA_ARGS__))

/*
 * _SAFEMACRO_DEFINE(name0, val0, ...)
 * => __auto_type name0 = (val0); ...
 */
#define _SAFEMACRO_DEFINE_RECUR() _SAFEMACRO_DEFINE_AUX
#define _SAFEMACRO_DEFINE_AUX(NAME, VAL, ...) \
	__auto_type NAME = (VAL); \
	__VA_OPT__(_SAFEMACRO_DEFER(_SAFEMACRO_DEFINE_RECUR)(__VA_ARGS__))
#define _SAFEMACRO_DEFINE(...) \
	_SAFEMACRO_EXPAND(_SAFEMACRO_DEFINE_AUX(__VA_ARGS__))

/*
 * _SAFEMACRO_LET(BODY, name0, val0, ...)
 * => ({ __auto_type name0 = (val0); BODY })
 */
#define _SAFEMACRO_LET(BODY, ...) ({ \
	__VA_OPT__(_SAFEMACRO_DEFINE(__VA_ARGS__)) \
	BODY; \
})

#define _SAFEMACRO_APPLY(MACRO, ...) \
	MACRO(__VA_ARGS__)

/*
 * _SAFEMACRO_SUBST(MACRO, name0, val0, ...) \
 * => _SAFEMACRO_LET(MACRO(name0, ...), name0, val0, ...)
 */
#define _SAFEMACRO_SUBST(MACRO, ...) \
	_SAFEMACRO_LET(_SAFEMACRO_APPLY(MACRO, __VA_OPT__(_SAFEMACRO_UNZIP_NAMES(__VA_ARGS__))), __VA_ARGS__)

/*
 * _SAFEMACRO_CONCAT(str0, str1)
 * => str0str1
 */
#define _SAFEMACRO_CONCAT_AUX(A, B) A ## B
#define _SAFEMACRO_CONCAT(A, B) _SAFEMACRO_CONCAT_AUX(A, B)

/*
 * _SAFEMACRO_FRESH_NAME()
 * => _safemacro_var0
 */
#define _SAFEMACRO_NAME_PREFIX _safemacro_var
#define _SAFEMACRO_FRESH_NAME() _SAFEMACRO_CONCAT(_SAFEMACRO_NAME_PREFIX, __COUNTER__)

#define _SAFEMACRO_MAKE_SAFE(MACRO, ...) \
	_SAFEMACRO_SUBST(MACRO, __VA_OPT__(_SAFEMACRO_ZIP_FRESH_NAMES(__VA_ARGS__)))

#define SAFEMACRO _SAFEMACRO_MAKE_SAFE

#endif /* _SAFEMACRO_H */
