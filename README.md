safemacro.h
===========

magically avoid double evaluation in function-like expression macros
--------------------------------------------------------------------

safemacro grew out of yak shaving expedition that started from a simple goal: write a simple `MAX` macro that compares
two numbers "correctly." As it turns out, macros in C have several properties that make this difficult. Let's start with
a naive attempt:

```c
#define MAX_UNSAFE(A, B) A >= B ? A : B
```

Most C programmers know that macros defined this way should always wrap their arguments in parentheses, and also
parenthesize the entire expression, in order to avoid problems with operator precedence. (See [1] if you are unfamiliar
with this problem.) So, a better attempt might be:

```c
#define MAX_SAFER(A, B) ((A) >= (B) ? (A) : (B))
```

and indeed, this approach is common, and good enough for most codebases. However, there is still a subtle issue:
`MAX_SAFER`'s arguments are evaluated twice. (Actually, because the ternary operator short-circuits, only one of the
arguments is evaluated twice, but I digress...) If `MAX_SAFER` is used on an expression with side effects, like `++i` or
`do_something(j)` this could lead to bugs.

Luckily, `safemacro.h` exports a single macro, called `SAFEMACRO`, that takes any simple function-like macro that
expands to an expression (like `MAX_UNSAFE`, above), and makes it "safe". For example:

```c
#define MAX(A, B) SAFEMACRO(MAX_UNSAFE, A, B)
```

Now,

```c
MAX(++i, do_something(j))
```

expands to

```c
({ __auto_type _safemacro_var0 = (++i);
   __auto_type _safemacro_var1 = (do_something(j));
   _safemacro_var0 >= _safemacro_var1 ? _safemacro_var0 : _safemacro_var1; })
```

`MAX`'s arguments are evaluated exactly once, behaving just like an inline function would - except that it's a macro,
and it's "generic" in the sense that can be used on any type that has a `>=` operator; we don't need `maxi`, `maxu`,
`maxf`, etc.

This works by combining ideas from several sources. [2] from the GCC documentation describes the problem and some
first-order solutions; [3] by Paul Fultz II gives us a method for doing recursion in the C preprocessor; [4] by David
Mazières uses `__VA_OPT__` to make writing recursive macros sane; and [5] by Likai Liu shows us how to write hygienic
macros in C. safemacro utilizes many concepts that will be familiar to programmers from functional languages, like
apply, let, zip and unzip, and even alpha substitution.

A note on portability: safemacro makes use of several features that are only available in GNU C or C2x (soon to be known
as C23): statement exprs, `__auto_type`, `__VA_OPT__`, and `__COUNTER__`. Statement expressions (the `({ a; b; })`
syntax) are essential and probably cannot be replaced. `__auto_type` could be replaced with `typeof`, but this would
still lead to double evaluation when an instance of a variably-modified type (e.g, a struct with a flexible array
member) is passed in.  `__VA_OPT__` could be removed by utilizing ideas described in [3]. I do not think `__COUNTER__`
can be replaced without horrible hacks.

Finally, if you need to define safe macros with more than six arguments, `_SAFEMACRO_EXPAND` must be modified. You can
add as many invocations of `_SAFEMACRO_EXPAND_ONCE` as you need.

[1]: https://stackoverflow.com/questions/7186504/c-macros-and-use-of-arguments-in-parentheses
[2]: https://gcc.gnu.org/onlinedocs/gcc/Statement-Exprs.html
[3]: https://github.com/pfultz2/Cloak/wiki/C-Preprocessor-tricks,-tips,-and-idioms
[4]: https://www.scs.stanford.edu/~dm/blog/va-opt.html
[5]: https://lifecs.likai.org/2016/07/c-preprocessor-hygienic-macros.html
